package ru.bakhtiyarov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractEntityDTO implements Serializable {

    @NotNull
    private String id = UUID.randomUUID().toString();

}
