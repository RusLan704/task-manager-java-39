package ru.bakhtiyarov.tm.endpoint;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.bakhtiyarov.tm.dto.TaskDTO;

import java.util.List;

@RequestMapping(value = "/rest/task")
public interface ITaskRestController {

    static ITaskRestController client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ITaskRestController.class, "http://localhost:8080/");
    }

    @Nullable
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO create(
            @RequestBody @Nullable TaskDTO taskDTO);

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TaskDTO> findAll();

    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO findById(
            @PathVariable("id") @Nullable String id
    );

    @PutMapping(value = "/updateById", produces = MediaType.APPLICATION_JSON_VALUE)
    TaskDTO updateTaskById(
            @RequestBody @Nullable TaskDTO taskDTO
    );

    @DeleteMapping(value = "/removeById/{id}")
    void removeOneById(
            @PathVariable("id") @Nullable String id
    );

}
