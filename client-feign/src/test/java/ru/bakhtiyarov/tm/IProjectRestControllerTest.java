package ru.bakhtiyarov.tm;

import org.junit.Assert;
import org.junit.Test;
import ru.bakhtiyarov.tm.dto.ProjectDTO;

import static ru.bakhtiyarov.tm.endpoint.IProjectRestController.client;

public class IProjectRestControllerTest {

    @Test
    public void findAllTest() {
        Assert.assertTrue(client().findAll().size() > 0);
    }

    @Test
    public void createTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setName("123");
        project.setDescription("435");
        client().create(project);
        ProjectDTO tempProject = client().findById(project.getId());
        Assert.assertEquals(tempProject.getName(), "123");
        Assert.assertEquals(tempProject.getDescription(), "435");
        client().removeOneById(project.getId());
    }

    @Test
    public void removeTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setName("123");
        project.setDescription("123");
        client().create(project);
        client().removeOneById(project.getId());
        Assert.assertFalse(client().findAll().contains(project));
    }

    @Test
    public void updateTest() {
        final ProjectDTO project = new ProjectDTO();
        project.setName("123");
        project.setDescription("345");
        client().create(project);
        project.setName("111");
        project.setDescription("000");
        client().updateProjectById(project);
        ProjectDTO projectUpdated = client().findById(project.getId());
        Assert.assertEquals(projectUpdated.getName(), "111");
        Assert.assertEquals(projectUpdated.getDescription(), "000");
        client().removeOneById(project.getId());
    }

}
