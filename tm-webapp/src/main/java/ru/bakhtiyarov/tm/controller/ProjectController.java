package ru.bakhtiyarov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.converter.IProjectConverter;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.enumeration.Status;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectConverter projectConverter;

    @NotNull
    @Autowired
    public ProjectController(
            @NotNull final IProjectService projectService,
            @NotNull IProjectConverter projectConverter
    ) {
        this.projectService = projectService;
        this.projectConverter = projectConverter;
    }

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @GetMapping("/list")
    public ModelAndView list() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-list");
        List<ProjectDTO> projects = projectService
                .findAll()
                .stream()
                .map(projectConverter::toDTO)
                .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-create");
        modelAndView.addObject("project", new ProjectDTO());
        return modelAndView;
    }

    @PostMapping("/create")
    public String create(@ModelAttribute("project") ProjectDTO project) {
        projectService.save(projectConverter.toEntity(project));
        return "redirect:/projects/list";
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id
    ) {
        projectService.removeOneById(id);
        return new ModelAndView("redirect:/projects/list");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-update");
        @Nullable final Project project = projectService.findById(id);
        modelAndView.addObject("project", projectConverter.toDTO(project));
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public String update(
            @ModelAttribute("project") ProjectDTO project
    ) {
        projectService.updateProjectById(
                project.getId(), project.getName(),
                project.getDescription(), project.getStatus(),
                project.getStartDate(), project.getFinishDate()
        );
        return "redirect:/projects/list";
    }

}