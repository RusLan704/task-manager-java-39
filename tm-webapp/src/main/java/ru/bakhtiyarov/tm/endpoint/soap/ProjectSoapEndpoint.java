package ru.bakhtiyarov.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.converter.IProjectConverter;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Component
@WebService
public class ProjectSoapEndpoint {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IProjectConverter projectConverter;

    @NotNull
    @Autowired
    public ProjectSoapEndpoint(
            @NotNull final IProjectService projectService,
            @NotNull final  IProjectConverter projectConverter
    ) {
        this.projectService = projectService;
        this.projectConverter = projectConverter;
    }

    @Nullable
    @WebMethod
    public ProjectDTO createProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @Nullable final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = projectService.save(projectConverter.toEntity(projectDTO));
        return projectConverter.toDTO(project);
    }

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects() {
        List<Project> projects = projectService.findAll();
        return projects
                .stream()
                .map(projectConverter::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        return projectConverter.toDTO(projectService.findById(id));
    }

    @Nullable
    @WebMethod
    public ProjectDTO updateProjectById(
            @WebParam(name = "projectDTO", partName = "projectDTO") @Nullable final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = projectService.updateProjectById(
                projectDTO.getId(),
                projectDTO.getName(),
                projectDTO.getDescription(),
                projectDTO.getStatus(),
                projectDTO.getStartDate(),
                projectDTO.getFinishDate()
        );
        return projectConverter.toDTO(project);
    }

    @WebMethod
    public void removeProjectOneById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        projectService.removeOneById(id);
    }

}
